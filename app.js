var points = [];
var setPixel = []
var sortedPixel = []
var corners = []
var pixelpoints = []


function convert(){
    image = document.getElementById('capturedimage');
    var canvas = document.getElementById("test");
    ctx = canvas.getContext("2d");

    canvas.width = image.width;
    canvas.height = image.height;

   ctx.drawImage(image, 0, 0, image.width, image.height);
   const scannedImage = ctx.getImageData(0, 0, canvas.width, canvas.height)
   const originalImage = ctx.getImageData(0, 0, canvas.width, canvas.height)

   class PixelPoint {
    constructor(img,value,height){
        this.r = img[value]
        this.g = img[value+1]
        this.b = img[value+2]
        this.a = img[value+3]
        this.img = img
        this.value = value
        this.height = height
    }

    getXCoordinate(){
        const imgheight = canvas.width * 4
        if(this.height == 0) {
            const x = this.value/4
            return x
        } else {
            const x = (this.value - (this.height * imgheight))/4
            return x
        }
    }

    getYCoordinate(){
        return this.height
    }

    getColor(){
        const avg = (this.r + this.g + this.b)/3
        return avg
    }
}

class Pixel {
    constructor(x,y,orientation){
        this.x = x,
        this.y = y,
        this.orientation = orientation
    }

    getPixelColor(data){
        const total = data[this.x] + data[this.x+1] + data[this.x+2]
        const avg = total/3
        return avg
    }
    
}

function Point(x,y){
    this.x = x;
    this.y = y;
}

function getRGB(data, value){
    const total = data[value] + data[value+1] + data[value+2]
    const avg = total/3
    return avg
}

function convertGreyscale (src){
    const scannedData = src.data;
    for(let i = 0; i < scannedData.length; i += 4){
        const totalvalue = scannedData[i] + scannedData[i+1] + scannedData[i+2]
        const totalavg = totalvalue/3;
        scannedData[i] = totalavg;
        scannedData[i+1] = totalavg;
        scannedData[i+2] = totalavg;
    }

    return scannedData
}

function convertBinary(src){
    const scannedData = src.data;
    const threshold = 350
    let newColor = 0;
    for(let i = 0; i < scannedData.length; i += 4){
        const totalvalue = scannedData[i] + scannedData[i+1] + scannedData[i+2]
        totalvalue > threshold ? newColor = 255: newColor = 0
        scannedData[i] = newColor
        scannedData[i+1] = newColor;
        scannedData[i+2] = newColor;
    }

    return scannedData
}

function findContour(src){ 
    const scannedData = src.data
    const width = canvas.width * 4;
    var startpixel = new Pixel();
    var currentpixel = new Pixel()
    let check = false

    for(let i=0; i<width; i+=4){
        let count = 0;
        if(check) break
        for(let j=i; count<canvas.height; count++){
            const total = scannedData[j] + scannedData[j+1] + scannedData[j+2]
            const avg = total/3
            // console.log(count + "," +  j + " color: " + avg)
            if(avg == 255){
                startpixel.x = j;
                startpixel.y = count;
                startpixel.orientation = "south"
                setPixel.push(new Point(j, count))

                currentpixel.x = j-4;
                currentpixel.y = count
                currentpixel.orientation = "west"
                check = true
                break
            }
            j += width
        }
    }
    while(currentpixel.x != startpixel.x){
            switch (currentpixel.orientation) {
                case "north":
                    if(currentpixel.getPixelColor(scannedData) == 255){
                        setPixel.push(new Point(currentpixel.x,currentpixel.y))
                        currentpixel.x += 4;
                        currentpixel.orientation = "east";
                    } else {
                        currentpixel.x -= 4;
                        currentpixel.orientation = "west"
                    }
                    break;
                case "west":
                    if(currentpixel.getPixelColor(scannedData) == 255){
                        setPixel.push(new Point(currentpixel.x,currentpixel.y))
                        currentpixel.x = currentpixel.x - width;
                        currentpixel.y--;
                        currentpixel.orientation = "north";

                    } else {
                        currentpixel.x = currentpixel.x + width;
                        currentpixel.y++;
                        currentpixel.orientation = "south";
                    }
                    break;
                case "east":
                    if(currentpixel.getPixelColor(scannedData) == 255){
                        setPixel.push(new Point(currentpixel.x,currentpixel.y))
                        currentpixel.x = currentpixel.x + width
                        currentpixel.y++
                        currentpixel.orientation = "south"

                    } else {
                        currentpixel.x = currentpixel.x - width;
                        currentpixel.y--;
                        currentpixel.orientation = "north";
                    }
                    break;
                case "south":
                    if(currentpixel.getPixelColor(scannedData) == 255){
                        setPixel.push(new Point(currentpixel.x,currentpixel.y))
                        currentpixel.x -= 4;
                        currentpixel.orientation = "west"

                    } else {
                        currentpixel.x += 4;
                        currentpixel.orientation = "east";
                    }
                    break;
                default:
                    break;
            }
    }
}

function drawContour(temp){
    tempData = temp.data
    for(let i = 0; i < tempData.length; i += 4){
        tempData[i] = 0;
        tempData[i+1] = 0;
        tempData[i+2] = 0;
    }

    setPixel.map((value) => {
        tempData[value.x] = 255;
        tempData[value.x+1] = 255;
        tempData[value.x+2] = 255
    })
    return tempData
}

function convertpixelpoints(img){
    setPixel.forEach((value)=>{
        pixelpoints.push(new PixelPoint(img, value.x, value.y))
    })
}

function findCenter(){
    let sumx = 0;
    let sumy = 0
    pixelpoints.forEach((value)=>{
        const x = value.getXCoordinate()
        const y = value.getYCoordinate()
        sumx = sumx + x;
        sumy = sumy + y;
    })

    const avgx = Math.floor(sumx/pixelpoints.length)
    const avgy = Math.floor(sumy/pixelpoints.length)

    const centerpoint = {
        x: avgx,
        y: avgy,
    }

    return centerpoint
}

function drawCenter(img, center){
    const imgHeight = canvas.width * 4
    const imgData = img.data;
    const imgx = (center.x) * 4;
    const imgy = (center.y) * imgHeight;
    const data = imgx + imgy


    imgData[data] = 255
    imgData[data+1] = 255
    imgData[data+2] = 255

    const newcenter = new PixelPoint(imgData, data, center.y)
    return newcenter
}

function getDistance(A,B){
    const xdistance = Math.pow((A.pixelpoint.getXCoordinate() - B.pixelpoint.getXCoordinate()),2)
    const ydistance = Math.pow((A.pixelpoint.getYCoordinate() - B.pixelpoint.getYCoordinate()),2)
    const total = Math.sqrt(xdistance+ydistance)
    return total
}


function sortPixel(center,img){
    class distancepixel {
        constructor(pixelpoint, distance){
            this.pixelpoint = pixelpoint,
            this.distance = distance
        }
    }

    pixelpoints.forEach((value)=>{
        const xdistance = Math.pow((value.getXCoordinate() - center.getXCoordinate()),2)
        const ydistance = Math.pow((value.getYCoordinate() - center.getYCoordinate()),2)
        const totaldistance = Math.sqrt(xdistance+ydistance)

        sortedPixel.push(new distancepixel(value, totaldistance))
    })

    sortedPixel.sort(function(a,b){
        if(a.distance > b.distance) return -1;
        if(a.distance < b.distance) return 1;
        return 0;
    })
   
    corners.push(sortedPixel[0])

    sortedPixel.forEach((p) => {
        if(corners.length === 0){
            corners.push(p)
        } else {
            let valid = true
            corners.forEach((c)=>{
                const distance = getDistance(c,p)
                if(distance < corners[0].distance){
                    valid = false
                }
            })
            if(valid){
                corners.push(p)
            }
        }
    })

}

   scannedImage.data = convertGreyscale(scannedImage)
   scannedImage.data = convertBinary(scannedImage)
   findContour(scannedImage, canvas.height, canvas.width)
   originalImage.data = drawContour(originalImage)
   convertpixelpoints(originalImage.data)
   const center = findCenter()
   const centerpixel = drawCenter(originalImage,center)
   sortPixel(centerpixel, originalImage.data)
   console.log(corners)


   ctx.putImageData(originalImage, 0,0)




}